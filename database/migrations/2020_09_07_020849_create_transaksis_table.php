<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->bigIncrements('id_transaksi');
            $table->string('kode_transaksi')->uniqid();
            $table->unsignedTinyInteger('buku_id')->nullable()->index();
            $table->unsignedTinyInteger('create_id')->nullable()->index();
            $table->string('nama_pembeli', 50);
            $table->date('tgl_transaksi', 50);
            $table->string('harga', 50);
            $table->string('jumlah', 50);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
