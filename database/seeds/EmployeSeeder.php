<?php

use Illuminate\Database\Seeder;
use App\Model\EmployeesModel;
use App\Model\CompanyModel;
use Faker\Factory as Faker;


class EmployeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = CompanyModel::all();

        foreach ($company as $key => $value) {
            $id_company =  $value->id_company;
            $faker = Faker::create();
            for ($i=0; $i < 20; $i++) { 
                $user = EmployeesModel::create([
                    'name' => $faker->name,
                    'email' => $faker->freeEmail,
                    'company_id' => $id_company
                ]);
            }
            
            
        }
       
    }
}
