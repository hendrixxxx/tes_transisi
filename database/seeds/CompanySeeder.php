<?php

use Illuminate\Database\Seeder;
use App\Model\CompanyModel;
use Faker\Factory as Faker;
class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i < 10 ; $i++) { 
            $name = $faker->company;
            $company = CompanyModel::create([
                'name' => $name,
                'email' => $faker->companyEmail,
                'logo' => $name.'.png',
                'website' => $faker->freeEmailDomain, 
            ]);
        }
        
    }
}
