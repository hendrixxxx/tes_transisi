<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware' => ['auth']], function () {

    Route::get('/home', 'DasarPhpController@index')->name('home');
    Route::get('/bagian/1/dasar/{nomor}', 'DasarPhpController@index')->name('dasar_1');
    Route::get('/add/papan_catur', 'DasarPhpController@html')->name('add_papan_catur');
    Route::get('/check', 'DasarPhpController@dasar3')->name('check');

    Route::resource('/company', 'CompanyController');
    Route::get('/source/company', 'CompanyController@source')->name('source.company');
    Route::get('/image/{id}', 'CompanyController@image')->name('image.company');
    Route::post('/image/upload', 'CompanyController@image_store')->name('image.upload');
    Route::get('/image/destroy', 'CompanyController@image_destroy')->name('image.destroy');

    Route::resource('/employe', 'EmployeController');
    Route::get('/source/employe', 'EmployeController@source')->name('source.employe');


    Route::resource('/buku', 'BukuController');
    Route::get('/source/buku', 'BukuController@source')->name('source.buku');

    Route::resource('/transaksi', 'TransaksiController');
    Route::get('/source/transaksi', 'TransaksiController@source')->name('source.transaksi');

});