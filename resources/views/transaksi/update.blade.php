<form method="post" enctype="multipart/form-data" id="form_update">
    <div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Kode buku</label>
                <input type="text"value="{{ !empty($data->kode_buku) ? $data->kode_buku : '' }}" name="kode_buku" id="kode_buku" class="form-control">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Nama</label>
                <input type="email" {{!empty($show) ? 'readonly' : '' }} value="{{ !empty($data->name) ? $data->name : '' }}" name="name" id="name" class="form-control">
            </div>
        </div>    

    </div>
</div>

<div class="modal-footer justify-content-between">
    <button id="submit" type="submit" onclick="{{$update}}" class="btn btn-primary">Update</button>
</div>
</form>
