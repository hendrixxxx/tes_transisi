@extends('layouts.admin')

@section('title')
Transaksi
@endsection

@section('part')
Transaksi
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endpush

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Transaksi Data</h3>
    <button style="float:right" type='button' onclick='create()' class='btn btn-success' data-toggle='modal'
      data-target='#modal-default'>
      <i class='fa fa-plus'></i>
    </button>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <table id="table_transaksi" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Kode Transaksi</th>
          <th>Buku</th>
          <th>Nama Pembeli</th>
          <th>Tanggal Transaksi</th>
          <th>Harga</th>
          <th>Jumlah</th>
          {{-- <th>Cabang</th> --}}
          <th>Create By</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>

    </table>
  </div>
  <!-- /.card-body -->

  {{-- modal --}}
  <div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Form Create</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i style="display:none;" id="loading" class="fas fa-sync-alt fa-spin"></i>

            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="modal_form"></div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

</div>
@endsection

@push('js')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
@endpush

@push('script')
<script>
  const $table = $("#table_transaksi").DataTable({
        autoWidth: false,
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url : '{!! route("source.transaksi") !!}',
            dataSrc: 'data'
        },
        language: {
            paginate: {
                previous: "<i class='fa fa-chevron-left'></i>",
                next: "<i class='fa fa-chevron-right'></i>",
            }
        },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
            {data: 'kode_transaksi', name: 'kode_transaksi'},
            {data: 'buku', name: 'buku'},
            {data: 'nama_pembeli', name: 'nama_pembeli'},
            {data: 'tgl_transaksi', name: 'tgl_transaksi'},
            {data: 'harga', name: 'harga'},
            {data: 'jumlah', name: 'jumlah'},
            {data: 'create', name: 'create'},
            {data: 'action', name: 'action', searchable : false, orderable : false,className: 'text-center'}
        ],
        search: {
            "regex": true
        }
    });
    function create()
  {
    
      $.ajax({
          url: "{{route('transaksi.create')}}",
          cache: false,
          beforeSend: function() {   
              $('.modal-title').text('');
              $( "#form_create" ).remove();
              $( "#form_update" ).remove();            
              $( "#modal-default" ).show();            
              $( "#loading" ).show();
              $('.modal-title').text('Form Create');
          },
          success: function(html){
              $( "#loading" ).hide();                
              $( "#modal_form" ).append(html); 
              $table.ajax.reload()              
          }
      });

  }

  function edit(val)
  {
      $.ajax({
          url: window.location.origin+window.location.pathname+'/'+val+'/edit',
        //   data: {'id':val},          
          cache: false,
          beforeSend: function() {   
              $('.modal-title').text('');
              $( "#form_update" ).remove();
              $( "#form_create" ).remove();  
              $( "#modal-default" ).show();            
              $( "#loading" ).show();
              $('.modal-title').text('Form Update');
          },
          success: function(data){
              $( "#loading" ).hide();                
              $( "#modal_form" ).append(data);               
          }
      });
  }

  function show(val)
  {
      $.ajax({
          type: 'GET',
          url: window.location.origin+window.location.pathname+'/'+val,
        //   data: {'id':val},          
          cache: false,
          beforeSend: function() {   
              $('.modal-title').text('');
              $( "#form_update" ).remove();
              $( "#form_create" ).remove();  
              $( "#modal-default" ).show();            
              $( "#loading" ).show();
              $('.modal-title').text('Form Show');
          },
          success: function(data){
              $( "#loading" ).hide();                
              $( "#modal_form" ).append(data);               
          }
      });
  }

  function hapus(val)
  {
      $.ajax({
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url: window.location.origin+window.location.pathname+'/'+val,
          data: {'id':val},          
          cache: false,
          beforeSend: function() {   
             
          },
          success: function(data){
            $table.ajax.reload()               
          }
      });
  }

  function update(val)
  {
    var data = $('#form_update').serialize();

    $.ajax({
        type: 'put',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: window.location.origin+window.location.pathname+"/"+val,
        data: data,
        beforeSend: function (data) {
            console.log(data)
            $('#submit').prop('disabled',true);
        },
        success: function (data) {
          $('#modal-default').modal('toggle');
          $table.ajax.reload()
        },
        error :function( data ) {
          $('#submit').prop('disabled',false);
          if( data.status === 422 ) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors['errors'], function( index, value ) {             
              $('#form_update').Toasts('create', {                 
                title: 'Error Input'+index,
                autohide: true,
                class: 'bg-danger',
                delay: 2500,
                body: value
          
              })
            });
            
          }
        },
    });
  }


  function store()
  {
    var data = $('#form_create').serialize();
    $.ajax({
        type: 'post',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: window.location.origin+window.location.pathname,        
        dataType:'json',
        data: data, 
        async: true,
        beforeSend: function () {
            $('#submit').prop('disabled',true);
        },
        success: function (data) {
          $('#modal-default').modal('toggle');
          $table.ajax.reload()
        },
        error :function( data ) {
          $('#submit').prop('disabled',false);
          if( data.status === 422 ) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors['errors'], function( index, value ) {             
              $('#form_create').Toasts('create', {                 
                title: 'Error Input'+index,
                autohide: true,
                class: 'bg-danger',
                delay: 2500,
                body: value
          
              })
            });
            
          }
        },
    });
  }

 
</script>
@endpush