<form id="form_create">
    <div class="modal-body" id="modal_form">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Kode Buku</label>
                    <input type="text" {{ !empty($show) ? 'readonly' : '' }} value="{{ !empty($show) ? $data->kode_buku : '' }}" name="kode_buku" id="kode_buku" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Nama</label>
                    <input type="email" {{!empty($show) ? 'readonly' : '' }} value="{{ !empty($show) ? $data->name : '' }}" name="name" placeholder="example@gmail.com" id="name" class="form-control">
                </div>
            </div>    
        </div>
    </div>
@if (empty($show))
    <div class="modal-footer justify-content-between">
        <button id="submit" onclick="store()" type="button" class="btn btn-primary">Save</button>
    </div>
@endif
    
</form>

@push('script')
<script>
    $('#logo').change(function(){
           
           let reader = new FileReader();
       
           reader.onload = (e) => { 
       
             $('#image_preview_container').attr('src', e.target.result); 
           }
       console.log(reader.readAsDataURL(this.files[0]));
           reader.readAsDataURL(this.files[0]); 
         
          });
</script>
    
@endpush

