<form method="post" enctype="multipart/form-data" id="form_update">
    <div class="modal-body">
    <div class="row">
    <div class="col-md-6">
        <div class="form-group">
        <label for="">Name Company</label>
        <input type="text" value="{{$data->name}}" name="name" id="name" class="form-control">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
        <label for="">Email Company</label>
        <input type="email" value="{{$data->email}}" name="email" placeholder="example@gmail.com" id="website" class="form-control">
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
        <label for="">Website</label>
        <input type="text" value="{{$data->website}}" name="website" id="website" class="form-control">
        </div>
    </div>

    </div>
</div>

<div class="modal-footer justify-content-between">
    <button id="submit" type="submit" onclick="{{$update}}" class="btn btn-primary">Update</button>
</div>
</form>
