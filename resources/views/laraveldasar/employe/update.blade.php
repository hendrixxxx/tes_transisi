<form method="post" enctype="multipart/form-data" id="form_update">
    <div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Name</label>
                <input type="text"value="{{ !empty($data->name) ? $data->name : '' }}" name="name" id="name" class="form-control">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Email</label>
                <input type="email" {{!empty($show) ? 'readonly' : '' }} value="{{ !empty($data->email) ? $data->email : '' }}" name="email" placeholder="example@gmail.com" id="website" class="form-control">
            </div>
        </div>    

        <div class="col-md-12">
            <div class="form-group">
                <label>Company</label>
                <select name="company_id" {{!empty($show) ? 'readonly' : '' }} class="custom-select">
                @foreach ($company as $item)
                 <option {{!empty($data->company_id) ? ($data->company_id == $item->id_company) ? 'selected' : '': '' }} value="{{$item->id_company}}">{{$item->name}}</option>
                @endforeach
                </select>
            </div>
              
        </div>

    </div>
</div>

<div class="modal-footer justify-content-between">
    <button id="submit" type="submit" onclick="{{$update}}" class="btn btn-primary">Update</button>
</div>
</form>
