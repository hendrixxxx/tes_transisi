<form id="form_create">
    <div class="modal-body" id="modal_form">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" {{ !empty($show) ? 'readonly' : '' }} value="{{ !empty($show) ? $data->name : '' }}" name="name" id="name" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" {{!empty($show) ? 'readonly' : '' }} value="{{ !empty($show) ? $data->email : '' }}" name="email" placeholder="example@gmail.com" id="website" class="form-control">
                </div>
            </div>    

            <div class="col-md-12">
                <div class="form-group">
                    <label>Company</label>
                    <select name="company_id" {{!empty($show) ? 'disabled' : '' }} class="custom-select">
                    @foreach ($company as $item)
                     <option {{!empty($show) ? ($data->company_id == $item->id_company) ? 'selected' : '': '' }} value="{{$item->id_company}}">{{$item->name}}</option>
                    @endforeach
                    </select>
                </div>
                  
            </div>
        </div>
    </div>
@if (empty($show))
    <div class="modal-footer justify-content-between">
        <button id="submit" onclick="store()" type="button" class="btn btn-primary">Save</button>
    </div>
@endif
    
</form>

@push('script')
<script>
    $('#logo').change(function(){
           
           let reader = new FileReader();
       
           reader.onload = (e) => { 
       
             $('#image_preview_container').attr('src', e.target.result); 
           }
       console.log(reader.readAsDataURL(this.files[0]));
           reader.readAsDataURL(this.files[0]); 
         
          });
</script>
    
@endpush

