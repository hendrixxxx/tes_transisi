 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link logo-switch">
      {{-- <img src="{{ asset('assets/dist/img/Logo_Transisi.png')}}" class="brand-image-xs logo-xl" 
        style=" width: 50%;
                background-color: white;
                left: 24%;"
      > --}}
    </a>

    <!-- Sidebar -->
    <div class="sidebar">


      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          {{-- <li class="nav-header">PHP DASAR</li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-circle"></i>
              <p>
                Bagian 1
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('dasar_1','1')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dasar 1</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('dasar_1','2')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dasar 2</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('dasar_1','3')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dasar 3</p>
                </a>
              </li>
            </ul>
          </li>


          <li class="nav-header">LARAVEL DASAR</li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-circle"></i>
              <p>
                Bagian 2
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item has-treeview">
                <a href="{{route('company.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>
                      COMPANY
                    </p>
                  </a>
                </li>
                <li class="nav-item has-treeview">
                  <a href="{{route('employe.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>
                      EMPLOYE
                    </p>
                  </a>
                </li>
            </ul>
          </li> --}}
          
          <li class="nav-header">Master</li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-circle"></i>
              <p>
                Master Data  
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item has-treeview">
                <a href="{{route('buku.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>
                      Buku
                    </p>
                  </a>
                </li>
                <li class="nav-item has-treeview">
                  <a href="{{route('transaksi.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>
                      Transaksi
                    </p>
                  </a>
                </li>
                <li class="nav-item has-treeview">
                  <a href="{{route('company.index')}}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>
                        Cabang
                      </p>
                    </a>
                  </li>
                  <li class="nav-item has-treeview">
                    <a href="{{route('employe.index')}}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>
                        User
                      </p>
                    </a>
                  </li>
            </ul>
          </li>
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>