@extends('layouts.admin')

@section('title')
Bagian 1 || TRANSISI
@endsection

@section('part')
BAGIAN 1 PHP DASAR 3
@endsection

@push('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endpush

@section('content')
<div class="row">

    {{-- begin::soal 1 --}}
    <div class="col-md-6">
        <div class="card card-primary  collapsed-card">
            <div class="card-header">
                <h3 class="card-title">SOAL 1</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">

                <ul>
                    <li>
                        <b>Data yang diketahui</b>
                        <p>
                            $arr = [
                                <blockquote style="border-left: none;"> 
                                    ['f', 'g', 'h', 'i'],<br>
                                    ['j', 'k', 'p', 'q'],<br>
                                    ['r', 's', 't', 'u']
                                </blockquote>
                            ]
                        </p>
                    </li>


                    <li>
                        <b>Soal</b>
                    </li>
                    <ul>
                        <li>Masukkan Huruf yang ingin dicari</li>
                        <div class="input-group input-group-sm">
                            {{ Form::text('name', null, ['class' => 'form-control ','placeholder'=>'fghi','id'=>'input_check']) }}
                            <span class="input-group-append">
                                <button type="button" onclick="check()" id="check" class="btn btn-info btn-flat">Check!</button>
                            </span>
                        </div>
                    </ul>
                    <li><b>Jawaban</b></li>
                    <ul>
                        <li data-card-widget="card-refresh">
                            
                            <p>Hasilnya adalah : <b id = 'show'><i style="display:none;" id="loading" class="fas fa-sync-alt fa-spin"></i></b></p>   
                        </li>
                    </ul>
                </ul>


            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    {{-- end::soal 1 --}}

</div>
@endsection

@push('js')

<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
@endpush

@push('script')
<script>
    function check()
    {
        var input = $("#input_check").val();
      
        $.ajax({
            url: "{{url('/check')}}",
            data: {'input': input,},
            cache: false,
            beforeSend: function() {   
                $( "#result" ).remove();            
                $( "#loading" ).show();
            },
            success: function(html){
                $( "#loading" ).hide();                
                $( "#show" ).append('<b id="result">'+ html +'</b>');               
            }
        });

    }
</script>
    
@endpush