@extends('layouts.admin')

@section('title')
Bagian 1 || TRANSISI
@endsection

@section('part')
BAGIAN 1 PHP DASAR 2
@endsection

@push('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endpush

@section('content')
<div class="row">

    {{-- begin::soal 1 --}}
    <div class="col-md-6">
        <div class="card card-primary  collapsed-card">
            <div class="card-header">
                <h3 class="card-title">SOAL 1</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <ul>
                    <li>
                        <b>Data yang diketahui</b>
                    </li>
                         <img src="{{asset('assets/dist/img/papan_catur.png')}}" alt="">
                    <li>
                        <b>Soal</b>
                    </li>
                    <ul>
                        <li>Buatlah fungsi php dengan emenampilkan hasil yang sama seperti yang diatas</li>
                    </ul>
                    <li><b>Jawaban</b></li>
                    <ul>
                        <li id="papan_catur">
                            
                        </li>
                    </ul>
                </ul>


            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    {{-- end::soal 1 --}}

    {{-- begin::soal 2 --}}
    <div class="col-md-6">
        <div class="card card-success  collapsed-card">
            <div class="card-header">
                <h3 class="card-title">SOAL 2</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <ul>
                    <li>
                        <b>Data yang diketahui</b>
                    </li>
                        kalimat sebelum di <b>"Enkripsi"</b> : <b>{{$asli}}</b> 
                    <li>
                        <b>Soal</b>
                    </li>
                    <ul>
                        <li>
                            <p>Buatlah sebuah fungsi <b>“enkripsi”</b>, yang apabila diberikan input DFHKNQ akan memberikan output EDKGSK</p>
                        </li>
                    </ul>
                    <li><b>Jawaban</b></li>
                    <ul>
                        <li>
                            <b>{{$enkripsi}}</b>
                        </li>
                    </ul>
                </ul>


            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    {{-- end::soal 2 --}}

</div>
@endsection

@push('js')

<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
@endpush

@push('script')
<script>
$(document).ready(function(){
    $.ajax({
        url: "{{route('add_papan_catur')}}",
        cache: false,
        success: function(html){
            $( '#papan_catur').append(html);
        }
    });
});    
   
</script>
@endpush