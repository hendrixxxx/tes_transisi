@extends('layouts.admin')

@section('title')
Bagian 1 || TRANSISI
@endsection

@section('part')
BAGIAN 1 PHP DASAR 1
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endpush

@section('content')
<div class="row">
    {{-- begin::soal 1 --}}
    <div class="col-md-6">
        <div class="card card-primary collapsed-card">
            <div class="card-header">
                <h3 class="card-title">SOAL 1</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <ul>
                    <li>
                        <b>Data yang diketahui</b>
                    </li>
                        <p>Nilai ujian = 72, 65, 73, 78, 75, 74, 90, 81, 87, 65, 55, 69, 72, 78, 79, 91, 100, 40, 67, 77, 86</p>
                    <li>
                        <b>Soal</b>
                    </li>
                    <ul>
                        <li>
                            <b>a.</b> mencari nilai rata-rata
                        </li>
                        <li>
                            <b>b.</b> mencari 7 nilai tertinggi
                        </li>
                        <li>
                            <b>c.</b> mencari 7 nilai terendah
                        </li>
                    </ul>
                    <li>
                        <b>Jawaban</b>
                    </li>
                    <ul>
                        <li> <b>a.</b>
                            {{round($avg)}} / {{$avg}}
                        </li>

                        <li> <b>b.</b>

                            @foreach ($bigs as $item)
                            {{$item}} ,
                            @endforeach

                        </li>
                        <li> <b>c.</b>
                            @foreach ($smalls as $item)
                            {{$item}} ,
                            @endforeach
                        </li>
                    </ul>
                </ul>


            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    {{-- end::soal 1 --}}

    {{-- begin::soal 2 --}}
    <div class="col-md-6">
        <div class="card card-success  collapsed-card">
            <div class="card-header">
                <h3 class="card-title">SOAL 2</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <ul>
                    <li>
                        <b>Data yang diketahui</b>
                    </li>
                        <p> input “TranSISI” </p>
                    <li>
                        <b>Soal</b>
                    </li>
                    <ul>
                        <li><b>a.</b> menampilkan jumlah huruf yang berukuran besar</li>
                        <li><b>b.</b> menampilkan jumlah huruf yang berukuran kecil</li>
                    </ul>
                    <li><b>Jawaban</b></li>
                    <ul>
                        <li> <b>a.</b>
                            Huruf kecil : {{$huruf_kecil}}
                        </li>

                        <li> <b>b.</b>
                            Huruf besar : {{$huruf_besar}}
                        </li>
                    </ul>
                </ul>


            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    {{-- end::soal 2 --}}

    {{-- begin::soal 3 --}}
    <div class="col-md-6">
        <div class="card card-success  collapsed-card">
            <div class="card-header">
                <h3 class="card-title">SOAL 3</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <ul>
                    <li>
                        <b>Data yang diketahui</b>
                    </li>
                        <p> input “Jakarta adalah ibukota negara Republik Indonesia” </p>
                    <li>
                        <b>Soal</b>
                    </li>
                    <ul>
                        <li><b>a.</b> menampilkan <b>Unigram</b> berdasarkan inputan</li>
                        <li><b>b.</b> menampilkan <b>Bigram</b> berdasarkan inputan</li>
                        <li><b>c.</b> menampilkan <b>Trigram</b>Trigram berdasarkan inputan</li>
                    </ul>
                    <li><b>Jawaban</b></li>
                    <ul>
                        <li> <b>a.</b>
                            {{$unigram}}
                        </li>

                        <li> <b>b.</b>
                            {{$bigram}}
                        </li>
                        </li>

                        <li> <b>c.</b>
                            {{$trigram}}
                        </li>
                    </ul>
                </ul>


            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    {{-- end::soal 3 --}}
</div>
@endsection

@push('js')

<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
@endpush

@push('script')

@endpush