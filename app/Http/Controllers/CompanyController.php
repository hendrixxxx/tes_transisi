<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Model\CompanyModel;
use App\Http\Requests\CompanyRequest;
use App\Http\Requests\ImageRequest;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class CompanyController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        
        return view('laraveldasar.company');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('laraveldasar.company.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {        
        $create = CompanyModel::create($request->all());
        return $create;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show = true;
        $data = CompanyModel::find($id);
        $path = Storage::url($data->logo);
        return view('laraveldasar.company.form',compact('data','show','path'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = CompanyModel::find($id);
        $update=  "update(".$id.")";        
        return view('laraveldasar.company.update',compact('data','update'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, $id)
    {
       
       $update = CompanyModel::where('id_company',$id)->update($request->all());
        return $update;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = CompanyModel::find($id);
        $deleted = Storage::delete('company/'.$destroy->logo);
        $destroy->delete();
        return  $destroy;
    }


    public function image($id)
    {
        $data = CompanyModel::find($id);
        return view('laraveldasar.company.image',compact('data'));
    }



    public function image_store( ImageRequest $request)
    {   
        
        if( !empty( $request->file('file') ) ) {
            
            $image      = $request->file('file');
            $ext        = $image->getClientOriginalExtension();
            $filename   = Carbon::now()->timestamp.'.'.$ext;


            // destroy
            $name_image = CompanyModel::find($request->id_company);            
            $deleted = Storage::delete('company/'.$name_image->logo);
           
           
            // add  
            $image_resize = Image::make($image->getRealPath());            
            $image_resize = $image_resize->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                    })->encode('png','jpg','jpeg','gif');
            $store = Storage::put('company/'.$filename, $image_resize->__toString());  


            // update
            CompanyModel::where('id_company',$request->id_company)->update(['logo'=>$filename]);
            return response()->json(['success'=> 'IMAGE SAVE']);  

        }
        
        
      
    }

    public function source()
    {        
        $query = CompanyModel::query()->latest('updated_at');
       
        return DataTables::eloquent($query)
        ->addIndexColumn()
        ->addColumn('action', function($row){
            $show =  'show('.$row->id_company.')';
            $edit =  'edit('.$row->id_company.')';
            $hapus =  'hapus('.$row->id_company.')';
            $image =  'image('.$row->id_company.')';
            $btn = "<div class='btn-group'>
                        <button type='button' onclick = ". $show." class='btn bg-gradient-success' data-toggle='modal' data-target='#modal-default'>
                            <i class='fa fa-eye'></i>
                        </button> 
                        <button type='button' onclick = ". $image." class='btn bg-gradient-info'>
                            <i class='fa fa-camera' aria-hidden='true'></i>
                        </button>                    
                        <button type='button' onclick = ". $edit." class='btn bg-gradient-warning' data-toggle='modal' data-target='#modal-default'>
                            <i class='fa fa-pencil-alt'></i>
                        </button>
                        
                        <button type='button' class='btn bg-gradient-danger' onclick = ". $hapus.">
                            <i class='fa fa-trash-alt'></i>
                        </button>
                    </div>";
            return $btn;
        })
        ->addColumn('image', function ($row) { 
            
            $path = Storage::url($row->logo);
            return '<img src="'.$path.'" alt="">';
        })
        ->rawColumns(['action','image'])
        ->toJson();
    }
}
