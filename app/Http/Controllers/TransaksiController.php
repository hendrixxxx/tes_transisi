<?php

namespace App\Http\Controllers;

use App\Transaksi;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Auth;

class TransaksiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('transaksi.table');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $buku = Transaksi::all();
        return view('transaksi.form',compact('buku'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Transaksi::create($request->all());
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show = true;
        $data = Transaksi::find($id);
        return view('transaksi.form',compact('data','show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Transaksi::find($id);
        $update=  "update(".$id.")";
        return view('transaksi.update',compact('data','update'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(request $request, $id)
    {
        $update = Transaksi::where('id_buku',$id)->update($request->all());
        return $update;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Transaksi::destroy($id);
        return  $destroy;
    }

    public function source()
    {        
        $query = Transaksi::query()->latest('updated_at');
  
        return DataTables::eloquent($query)      
        ->addIndexColumn()
        ->addColumn('buku', function ($row) {             
            return $row->buku->name;
        })
        ->addColumn('create', function ($row) {             
            return $row->user->name;
        })
        ->addColumn('action', function($row){
            $show =  'show("'.$row->id_transaksi.'")';
            $edit =  'edit('.$row->id_transaksi.')';
            $hapus =  'hapus('.$row->id_transaksi.')';
            $btn = "<div class='btn-group'>
                        <button type='button' onclick = ". $show." class='btn bg-gradient-info' data-toggle='modal' data-target='#modal-default'>
                            <i class='fa fa-eye'></i>
                        </button>                    
                        <button type='button' onclick = ". $edit." class='btn bg-gradient-warning' data-toggle='modal' data-target='#modal-default'>
                            <i class='fa fa-pencil-alt'></i>
                        </button>
                        
                        <button type='button' class='btn bg-gradient-danger' onclick = ". $hapus.">
                            <i class='fa fa-trash-alt'></i>
                        </button>
                    </div>";
            return $btn;
        })
        ->rawColumns(['action'])
        ->toJson();
    }
}
