<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class DasarPhpController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($nomer)
    {

        if ( $nomer == 1 ) {

            return $this->dasar1();

        } elseif ( $nomer == 2 ) {

            return $this->dasar2(); 

         }elseif ( $nomer == 3 ) {

            return view('phpdasar.dasar_3');

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dasar1()
    {
              
        // begin::SOAL 1
            $nilai = [  '72','65','73','78','75',
                        '74','90','81','87','65',
                        '55','69','72','78','79',
                        '91','100','40','67','77','86'];

            $collection = collect($nilai);

            $smalls = $collection->sort()->take(7);  
            $bigs = $collection->sortDesc()->take(7);
            $avg = $collection->avg();
        // end::SOAL 1
        
        // begin::SOAL 2
            $string    = "TranSISI";
            $lowerCase = strtolower($string);
            $upperCase = strtoupper($string);
            
            $huruf_kecil = similar_text($string, $lowerCase);
            $huruf_besar = similar_text($string, $upperCase);
        // end::SOAL 2
    
        // begin::SOAL 3
            $input = "Jakarta adalah ibukota negara Republik Indonesia";
            $explod = explode(' ',$input);
            
            $unigram = $explod[0].','.$explod[1].','.$explod[2].','.$explod[3].','.$explod[4].','.$explod[5];
            $bigram = $explod[0].' '.$explod[1].','.$explod[2].' '.$explod[3].','.$explod[4].' '.$explod[5];
            $trigram = $explod[0].' '.$explod[1].' '.$explod[2].','.$explod[3].' '.$explod[4].' '.$explod[5];
        // end::SOAL 3

        return view('phpdasar.dasar_1', 
            compact('smalls','bigs','avg',
                    'huruf_kecil','huruf_besar',
                    'unigram','bigram','trigram'
        )); 
    }

    public function dasar2()
    {
        // begin::SOAL 1
            
            // In public function html(){}

        // end::SOAL 1

        // begin::SOAL 2
        
            $kalimat = "DFHKNQ";
            
            $key = [1,-2,3,-4,5,-6];

            for( $i = 0 ; $i < strlen($kalimat) ; $i++ ) {

                $kode[$i] = ord( $kalimat[$i] ); //rubah ASCII ke desimal
                $b[$i] = ( $kode[$i] + $key[$i] ) % 256; //proses enkripsi
                $c[$i] = chr( $b[$i] ); //rubah desimal ke ASCII
            }
            
            $asli = '';
            for ($i = 0 ; $i < strlen($kalimat) ; $i++) { 

                $asli .= $kalimat[$i];

            }


            $enkripsi = '';
            for ( $i = 0 ; $i < strlen($kalimat) ; $i++ ) { 
                $enkripsi .= $c[$i];
            }
        // end::SOAL 2
        return view('phpdasar.dasar_2',compact('asli','enkripsi')); 
    }
    

    public function dasar3(Request $request)
    {
        // Dasar 3

        $str = $request->input;
        $arr = [
                    ['f', 'g', 'h', 'i'],
                    ['j', 'k', 'p', 'q'],
                    ['r', 's', 't', 'u']
                    ];
        $split = str_split($str);
        $check = []; 
        foreach ($split as  $string) {
              
            foreach ($arr as  $value) {
              
                foreach ($value as $item) {

                    $validation =  ($item == $string);
                    if ($validation == true) {

                        $check [] = $validation;

                    }

                }
               
            }
        }
        
        $str_split = collect($split)->count();
        $str_check = collect($check)->count();

       if ( $str_check > $str_split || $str_check == $str_split ) {

            $retun = 'True';

       } else {

            $retun = 'False';

       }
       return  $retun;
        
    }

    public function html()
    {
        // begin::SOAL 1
            
            $text = '';
            $jumlah = 8;
            $text .= '<table border=1>';
            $no = 1;
            for( $n = 0; $n < 8; $n++ ) {
                $text .= '<tr>';
                for( $m = 0; $m < 8; $m++ ) {
            
                    $warna = '';
                    $val = [0,1,2,5,7,10,11,13,14,17,19,22,23,25,26,29,31,34,35,37,38,41,43,46,47,49,50,53,55,58,59,61,62]; 
                    $check =  array_search($no,$val);
                    
                    if ( $check ) {
                        $warna = '#000';
                        $style = 'style="color: white;"';
                        
                    } else {
                        $warna = '#fff';
                        $style = '';
                    }
                    $text .= '<td width=30 '.$style.' height=30 bgcolor='.$warna.'>'.$no.'</td>';
                    $no++;
                }
                $text .= '</tr>';
            }

            $text .= '</table>';
            $catur_acak = $text;
            return $catur_acak;
    }
    
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}