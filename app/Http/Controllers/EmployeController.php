<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Model\EmployeesModel;
use App\Model\CompanyModel;
use App\Http\Requests\EmployeRequest;


class EmployeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('laraveldasar.employe');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = CompanyModel::all();
        return view('laraveldasar.employe.form',compact('company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeRequest $request)
    {
        $data = EmployeesModel::create($request->all());
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show = true;
        $data = EmployeesModel::find($id);
        $company = CompanyModel::all();
        return view('laraveldasar.employe.form',compact('data','show','company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = EmployeesModel::find($id);
        $company = CompanyModel::all();
        $update=  "update(".$id.")";
        
        return view('laraveldasar.employe.update',compact('data','update','company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeRequest $request, $id)
    {
        $update = EmployeesModel::where('id_employe',$id)->update($request->all());
        return $update;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = EmployeesModel::destroy($id);
        return  $destroy;
    }

    public function source()
    {        
        $query = EmployeesModel::query()->latest('updated_at');
  
        return DataTables::eloquent($query)      
        ->addIndexColumn()
        ->addColumn('company', function($row){
            if ( !empty( $row->company->name ) ) {
                $data = $row->company->name;
            } else {
                $data = '';
            }
            
            return $data;            
        })
        ->addColumn('action', function($row){
            $show =  'show("'.$row->id_employe.'")';
            $edit =  'edit('.$row->id_employe.')';
            $hapus =  'hapus('.$row->id_employe.')';
            $btn = "<div class='btn-group'>
                        <button type='button' onclick = ". $show." class='btn bg-gradient-info' data-toggle='modal' data-target='#modal-default'>
                            <i class='fa fa-eye'></i>
                        </button>                    
                        <button type='button' onclick = ". $edit." class='btn bg-gradient-warning' data-toggle='modal' data-target='#modal-default'>
                            <i class='fa fa-pencil-alt'></i>
                        </button>
                        
                        <button type='button' class='btn bg-gradient-danger' onclick = ". $hapus.">
                            <i class='fa fa-trash-alt'></i>
                        </button>
                    </div>";
            return $btn;
        })
        ->rawColumns(['action'])
        ->toJson();
    }
}
