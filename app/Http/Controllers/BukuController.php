<?php

namespace App\Http\Controllers;

use App\Buku;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class BukuController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('buku.table');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $buku = Buku::all();
        return view('buku.form',compact('buku'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Buku::create($request->all());
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show = true;
        $data = Buku::find($id);
        return view('buku.form',compact('data','show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = buku::find($id);
        $update=  "update(".$id.")";
        return view('buku.update',compact('data','update'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(request $request, $id)
    {
        $update = Buku::where('id_buku',$id)->update($request->all());
        return $update;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Buku::destroy($id);
        return  $destroy;
    }

    public function source()
    {        
        $query = Buku::query()->latest('updated_at');
  
        return DataTables::eloquent($query)      
        ->addIndexColumn()
        ->addColumn('action', function($row){
            $show =  'show("'.$row->id_buku.'")';
            $edit =  'edit('.$row->id_buku.')';
            $hapus =  'hapus('.$row->id_buku.')';
            $btn = "<div class='btn-group'>
                        <button type='button' onclick = ". $show." class='btn bg-gradient-info' data-toggle='modal' data-target='#modal-default'>
                            <i class='fa fa-eye'></i>
                        </button>                    
                        <button type='button' onclick = ". $edit." class='btn bg-gradient-warning' data-toggle='modal' data-target='#modal-default'>
                            <i class='fa fa-pencil-alt'></i>
                        </button>
                        
                        <button type='button' class='btn bg-gradient-danger' onclick = ". $hapus.">
                            <i class='fa fa-trash-alt'></i>
                        </button>
                    </div>";
            return $btn;
        })
        ->rawColumns(['action'])
        ->toJson();
    }

}
