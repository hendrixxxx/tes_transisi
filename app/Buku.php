<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $fillable = ['kode_buku', 'name'];

    protected $primaryKey = 'id_buku';

}
