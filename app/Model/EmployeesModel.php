<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmployeesModel extends Model
{
    protected $table = 'mst_employees';
    protected $fillable = ['name','email','company_id'];
    protected $primaryKey ='id_employe';

    
    public function company()
    {
        return $this->belongsTo('App\Model\CompanyModel', 'company_id', 'id_company');
    }
}
