<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CompanyModel extends Model
{
    protected $table = 'mst_company';
    protected $fillable = ['name','email','logo','website'];
    protected $primaryKey ='id_company';

    
    public function employe()
    {
        return $this->hasMany('App\Model\EmployeesModel', 'company_id', 'id_company');
    }
}
