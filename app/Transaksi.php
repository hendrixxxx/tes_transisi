<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $fillable = ['kode_transaksi', 'buku_id','create_id','nama_pembeli','tgl_transaksi','harga','jumlah'];

    protected $primaryKey = 'id_btransaksi ';

    public function buku()
    {
        return $this->belongsTo(Buku::class,'buku_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'create_id');
    }

}
